{
	"logger_file_name":"C:\\r3tc.log" //R3 Thin Client server log file
	,"pgsql":{
		"host":"192.168.1.30" //PostgreSQL IP address
		,"port":5432 //PostgreSQL listening port
		,"database":"r3tc" //R3 Thin Client database name
		,"schema":"public"
		,"dba_account":"postgres" //role used to connect to PostgreSQL
		,"dba_password":"myPgPassword" //password used to connect to PostgreSQL
	}
	,"oracle":{
		"host":"localhost" //host address or IP address
		,"port":1521 //listening port
		,"database":"D01" //database name
		,"dba_account":"superuser" //login account
		,"dba_password":"myOraclePassword" //login password
	}
	,"r3":{
		"host":"localhost" //host address or IP address
		,"system":"S01" //system ID
		,"instance":"00" //instance number
	}
}
