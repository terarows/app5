<chapter id="chapter-data-entry-rules">
	<title>Data Entry Rules</title>
	<para>Most meta data tables of <productname>&SystemName;</> are in <literal>master-detail</> (i.e., <literal>on-to-many</>) relationships. Their data records must be entered in <emphasis>top-down</> order. Take the following screen as an example,</para>
	<figure><title>Top-Down Data Entry</title>
		<mediaobject>
			<imageobject>
				<imagedata fileref="images/content/mzf5-sql.png" format="png">
			</imageobject>
		</mediaobject>
	</figure>
	<para>enter records in the following order:</para>
	<orderedlist>
		<listitem><para><literal>Report#</>, <literal>Used-In Screen</>, <literal>For Dot Matric Printer?</></para>
		<listitem><para><literal>Report Name</>, <literal>Component Report</>, <literal>Report User</></para></listitem>
		<listitem><para><literal>Data Sets</>, <literal>Sub Reports</></para></listitem>
		<listitem><para><literal>SQL Text</>, <literal>Parameters</></para></listitem>
	</orderedlist>
	<important>
		<para>When a master record is deleted, its associated detail records are cascadely deleted automatically. Following above example, if a specific <literal>report#</> is deleted, then its assocated <literal>Report Name</>, <literal>Component Reports</>, and <literal>Report User</> are automatically deleted; If a specific <literal>Component Reports</> is deleted, then its "linked" <literal>Data Sets</> and <literal>Sub Reports</> are also deleted.</para>
		<para>Before pushing <literal>delete record</> button on tool bar, make sure the cursor focus on any field belonging to the correct target record.</para>
	</important>
</chapter>
