# License Notice

##Terminologies

- R3 Thin Client:
  `R3 Thin Client` is a software created by `Tera Rows`.
- Resources:
	`Resources` include, but are not limited to, the following items:
	- documentation
	- database metadata also known as "data dictionary"
	- `R3 Thin Client` program files and configuration files
	- database structure of accounting journal
- customers:
	`Customers` refers to the organizations or individuals who are used to purchase `R3 Thin Client`.
- potential customers:
	`Potential customers` refers to the organizations or individuals who are evaluating the possibility of purchasing `R3 Thin Client`.

## Prohibited Usage Of Resources

Usage of these `resources` by organizations or individuals not belonging to the `readers` mentioned in the following part of this license notice are prohibited. In general, applying the system design techniques revealed in these `resources` to your own information systems design is strictly prohibited.

## Permitted Usage Of Resources

The following `readers` are granted the usage of the aforementioned `resources` free of charge:

- existing and potential customers of `Tera Rows`
- existing and potential consultants of `Tera Rows`' existing and potential customers

These `readers` are granted to read, use, copy, merge, archive, and publish of these `resources`, subject to the following conditions:

All part of this copyright notice must not be modified and shall be included in all copies of the `resources`.

THE `RESOURCES` IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE `RESOURCES` OR THE USE OR OTHER DEALINGS IN THE `RESOURCES`.
